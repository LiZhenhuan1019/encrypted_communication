#include <iostream>
#include <string>
#include <signal.h>
#include <unitypes.h>
#include <syslog.h>
#include <cstring>
#include <charconv>
#include "use_ssl.hpp"
#include "daemon.hpp"
#include "server.hpp"

bool will_exit = false;
fd_handler listen_fd(-1);
char success_response[] = "success\n";
char fail_respnose[] = "failed\n";
char address_str[INET6_ADDRSTRLEN]{};
std::uint16_t port;
char port_str[10]{};
void sigint_handler(int)
{
    will_exit = true;
    listen_fd.close_fd();
}
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::printf("usage: server PORT\n");
        return 0;
    }
    char *end_pos;
    unsigned long long_port = std::strtoul(argv[1], &end_pos, 10);
    if (*end_pos != '\0' || long_port >= 65536)
    {
        std::printf("usage: client SERVER PORT\n"
                    "    PORT should be a non-negative number less than 65536.\n");
        return 0;
    }
    signal(SIGINT, sigint_handler);
    try
    {
        printf("daemonizing.\n");
        int pid;
        switch (pid = daemonize())
        {
            case -1:
                return -1;
            case 0:
                break;
            default:
                printf("pid of daemon is %d\n", pid);
                return 0;
        }

        listen_fd = fd_handler(use_ssl::open_listener(std::uint16_t(long_port), 50, true));
        if (listen_fd.get_fd() < 0)
            throw std::runtime_error("open listen socket failed.\n");
        ssl_server server(listen_fd.get_fd());

        while (!will_exit)
        {
            server.serve([](ssl_connection const &conn, std::string_view request) mutable
                         {
                             syslog(LOG_DEBUG, "Received request from client: '%s'\n", request.data());
                             std::string_view post_head = "post-ip";
                             std::string_view get_head = "get-ip";
                             if (post_head == request)
                             {
                                 get_address_and_port(conn.addr, address_str, sizeof(address_str), port);
                                 std::to_chars_result result = std::to_chars(port_str, port_str + sizeof(port_str) - 1, port);
                                 if (result.ec == std::errc::value_too_large)
                                 {
                                     syslog(LOG_DEBUG, "port number to large.");
                                     return;
                                 }
                                 *result.ptr = '\0';
                                 if (SSL_write(conn.ssl, success_response, sizeof(success_response)) < 0)
                                     syslog(LOG_DEBUG, "SSL_write returns negative");

                             } else if (get_head == request)
                             {
                                 SSL_write(conn.ssl, address_str, std::strlen(address_str));
                                 SSL_write(conn.ssl, ":", 1);
                                 SSL_write(conn.ssl, port_str, std::strlen(port_str));
                                 SSL_write(conn.ssl, "\n", 1);
                             } else
                             {
                                 char respond[] = "unexpected request.\n";
                                 SSL_write(conn.ssl, respond, sizeof(respond));
                             }
                         });
        }
    }
    catch (std::runtime_error const &e)
    {
        syslog(LOG_ERR, "%s", e.what());
    }


    return 0;
}
