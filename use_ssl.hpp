#ifndef ENCRYPTED_COMMUNICATION_USE_SSL_HPP
#define ENCRYPTED_COMMUNICATION_USE_SSL_HPP
#include <cstdint>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/crypto.h>
#include <openssl/err.h>
#include <openssl/x509.h>
#include <netinet/in.h>
namespace use_ssl
{
    void initialize_ssl()
    {
        OPENSSL_init_ssl(0, nullptr);
    }
    SSL_CTX *create_server_contex()
    {
        SSL_CTX *ctx = SSL_CTX_new(TLS_server_method());
        if (ctx == nullptr)
        {
            ERR_print_errors_fp(stderr);
        }
        return ctx;
    }
    SSL_CTX *create_client_contex()
    {
        SSL_CTX *ctx = SSL_CTX_new(TLS_client_method());
        if (ctx == nullptr)
        {
            ERR_print_errors_fp(stderr);
        }
        return ctx;
    }
    void set_verify_peer(SSL_CTX *ctx)
    {
        SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, nullptr);
    }
    bool load_certificates(SSL_CTX *ctx, char const *cert_file, char const *private_key_file)
    {
        if (SSL_CTX_use_certificate_file(ctx, cert_file, SSL_FILETYPE_PEM) != 1)
        {
            ERR_print_errors_fp(stderr);
            return false;
        }
        if (SSL_CTX_use_PrivateKey_file(ctx, private_key_file, SSL_FILETYPE_PEM) != 1)
        {
            ERR_print_errors_fp(stderr);
            return false;
        }
        if (SSL_CTX_check_private_key(ctx) != 1)
        {
            ERR_print_errors_fp(stderr);
            return false;
        }
        return true;
    }
    int open_listener(std::uint16_t port, int backlog = 10, bool non_block = false)
    {
        if (backlog <= 0)
        {
            fprintf(stderr, "backlog of bind should be a positive number.\n");
        }
        int type = SOCK_STREAM;
        if (non_block)
            type |= SOCK_NONBLOCK;
        int listener = socket(AF_INET, type, 0);
        if (listener < 0)
        {
            fprintf(stderr, "socket error\n");
        }
        sockaddr_in addr{};
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
        if (::bind(listener, (struct sockaddr *) &addr, sizeof(addr)) != 0)
        {
            perror("can't bind port.");
            return -1;
        }
        if (listen(listener, 10) != 0)
        {
            perror("listen socket failed.");
            return -1;
        }
        return listener;
    }
    int open_client(char const *hostname, char const *port)
    {
        addrinfo hint{}, *info;
        hint.ai_family = AF_UNSPEC;
        hint.ai_socktype = SOCK_STREAM;
        hint.ai_protocol = 0;
        if (int ec = getaddrinfo(hostname, port, &hint, &info))
        {
            fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(ec));
        }
        int sfd;
        addrinfo *current = info;
        for (; current != nullptr; current = current->ai_next)
        {
            sfd = socket(current->ai_family, current->ai_socktype, current->ai_protocol);
            if (sfd == -1)
                continue;
            if (connect(sfd, current->ai_addr, current->ai_addrlen) != -1)
                break;
            int code = errno;
            close(sfd);
        }
        if (current == nullptr)
        {
            fprintf(stderr, "connect to host '%s'failed.\n", hostname);
            freeaddrinfo(info);
            return -1;
        }
        freeaddrinfo(info);
        return sfd;
    }
    void show_certs(SSL *ssl)
    {
        X509 *cert = SSL_get_peer_certificate(ssl);
        if (cert == nullptr)
        {
            printf("No certificates.\n");
            return;
        }
        printf("subject: ");
        X509_NAME_print_ex_fp(stdout, X509_get_subject_name(cert), 80, XN_FLAG_RFC2253);
        printf("\nissuer: ");
        X509_NAME_print_ex_fp(stdout, X509_get_subject_name(cert), 80, XN_FLAG_RFC2253);
        printf("\n");
        X509_free(cert);
    }

    bool verify_certs_with_pubkey(SSL *ssl, char const *pubkey_file)
    {
        FILE *file = fopen(pubkey_file, "r");
        if (file == nullptr)
        {
            fprintf(stderr, "open public key file failed.\n");
            return false;
        }
        EVP_PKEY *pkey = PEM_read_PUBKEY(file, nullptr, nullptr, nullptr);
        X509 *cert = SSL_get_peer_certificate(ssl);
        if (cert == nullptr)
        {
            fprintf(stderr, "verifying certs but found no certificates.\n");
            return false;
        }
        return X509_verify(cert, pkey) == 1;
    }

}


#endif //ENCRYPTED_COMMUNICATION_USE_SSL_HPP
