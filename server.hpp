#ifndef ENCRYPTED_COMMUNICATION_SERVER_HPP
#define ENCRYPTED_COMMUNICATION_SERVER_HPP

#include <unistd.h>
#include <cstdint>
#include <stdexcept>
#include <utility>
#include <syslog.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <vector>
#include <lzhlib/repository/value_repo.hpp>
#include "use_ssl.hpp"


void get_address_and_port(sockaddr const &addr, char *buffer, std::size_t buf_len, std::uint16_t &port)
{
    switch (addr.sa_family)
    {
    case AF_INET:
    {
        auto &addr_in = (sockaddr_in &) addr;
        inet_ntop(AF_INET, &addr_in.sin_addr, buffer, buf_len);
        port = ntohs(addr_in.sin_port);
        break;
    }

    case AF_INET6:
    {
        auto &addr_in6 = (sockaddr_in6 &) addr;
        inet_ntop(AF_INET6, &addr_in6.sin6_addr, buffer, buf_len);
        port = ntohs(addr_in6.sin6_port);
        break;
    }
    }
}

struct fd_handler
{
    explicit fd_handler(int fd) noexcept
        : fd(fd)
    {}
    fd_handler(fd_handler const &src) = delete;
    fd_handler(fd_handler &&src) noexcept
        : fd(std::exchange(src.fd, -1))
    {}
    fd_handler &operator=(fd_handler const &) = delete;
    fd_handler &operator=(fd_handler &&src) noexcept
    {
        std::swap(fd, src.fd);
        return *this;
    }
    ~fd_handler()
    {
        close(fd);
    }
    int get_fd() const
    {
        return fd;
    }
    void close_fd()
    {
        close(fd);
        fd = -1;
    }

private:
    int fd;
};

struct ssl_connection
{
    constexpr explicit ssl_connection(int fd, sockaddr const &addr = sockaddr{}, SSL *ssl = nullptr)
        : fd(fd), addr(addr), ssl(ssl)
    {}
    int fd = -1;
    sockaddr addr{};
    SSL *ssl = nullptr;
    bool accepted = false;
};

struct ssl_server_stat
{
    ssl_server_stat(int listen_fd, int max_events)
        : listen_fd(listen_fd), connections(max_events, ssl_connection(-1))
    {
        epoll_fd = epoll_create(10); // argument 10 is ignored.
        if (epoll_fd < 0)
            throw std::runtime_error("create epoll failed.");
        epoll_event added_event{};
        added_event.events = EPOLLIN | EPOLLET;
        auto id = connections.assign_object(ssl_connection{listen_fd});
        added_event.data.u64 = (std::uint64_t &) id;

        if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listen_fd, &added_event) < 0)
            throw std::runtime_error("epoll_ctl add listen fd failed.");
        use_ssl::initialize_ssl();
        ctx = use_ssl::create_server_contex();
        if (!use_ssl::load_certificates(ctx, "/home/lzh/.ssl/id_rsa.crt", "/home/lzh/.ssl/id_rsa"))
        {
            throw std::runtime_error("load certificate failed.\n");
        }
        use_ssl::set_verify_peer(ctx);

    }
    ssl_server_stat(ssl_server_stat const &) = delete;
    ssl_server_stat(ssl_server_stat &&src) noexcept
        : listen_fd(std::exchange(src.listen_fd, -1)), ctx(std::exchange(src.ctx, nullptr)), epoll_fd(std::exchange(src.epoll_fd, -1)), connections(std::move(src.connections))
    {}
    ssl_server_stat &operator=(ssl_server_stat const &) = delete;
    ssl_server_stat &operator=(ssl_server_stat &&rhs) noexcept
    {
        using std::swap;
        swap(listen_fd, rhs.listen_fd);
        swap(ctx, rhs.ctx);
        swap(epoll_fd, rhs.epoll_fd);
        swap(connections, rhs.connections);
        return *this;
    }
    ~ssl_server_stat()
    {
        SSL_CTX_free(ctx);
    }
    int listen_fd;
    SSL_CTX *ctx;
    int epoll_fd;
    lzhlib::value_repo<ssl_connection> connections;
};
struct ssl_server
{
    explicit ssl_server(int listen_fd, std::size_t max_events = 5000)
        : stat(listen_fd, max_events), events(max_events)
    {
    }
    ssl_server(ssl_server const &) = delete;
    ssl_server &operator=(ssl_server const &) = delete;

    template <typename Func>
    void serve(Func f)
    {
        int num_fds = epoll_wait(stat.epoll_fd, &events[0], events.size(), -1);
        if (num_fds < 0)
            syslog(LOG_DEBUG, "epoll_wait failed.");
        for (int i = 0; i < num_fds; ++i)
        {
            typename decltype(stat.connections)::id_t id = events[i].data.u64;
            auto &conn = stat.connections.get_object(id);
            if (conn.fd == stat.listen_fd)
            {
                while (true)
                {
                    epoll_event event{};
                    struct sockaddr addr{};
                    socklen_t len;
                    int communicate_fd = accept(stat.listen_fd, &addr, &len);
                    if (communicate_fd == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
                        break;
                    fcntl(communicate_fd, F_SETFL, O_NONBLOCK);
                    event.events = EPOLLIN | EPOLLET;
                    SSL *ssl = SSL_new(stat.ctx);
                    if (SSL_set_fd(ssl, communicate_fd) == 0)
                    {
                        ERR_print_errors_fp(stderr);
                        syslog(LOG_ERR, "set fd to ssl failed.");
                        SSL_free(ssl);
                        continue;
                    }
                    auto new_id = stat.connections.assign_object(ssl_connection{communicate_fd, addr, ssl});
                    static_assert(sizeof(id) == 8);
                    event.data.u64 = (std::uint64_t &) new_id;
                    if (epoll_ctl(stat.epoll_fd, EPOLL_CTL_ADD, communicate_fd, &event) < 0)
                    {
                        syslog(LOG_NOTICE, "epoll add failed.");
                    }
                    char address_str[INET6_ADDRSTRLEN]{};
                    std::uint16_t port;
                    get_address_and_port(addr, address_str, sizeof(address_str), port);
                    syslog(LOG_INFO, "accept from client %s:%hu", address_str, port);
                }
            } else
            {
                int error_code = 0;
                if (events[i].events == EPOLLIN)  // peer doesn't close connection.
                {
                    if (!conn.accepted)
                    {
                        if (int ret = SSL_accept(conn.ssl); ret <= 0)
                        {
                            ERR_print_errors_fp(stderr);
                            syslog(LOG_DEBUG, "ssl accept failed");
                            continue;
                        }
                        syslog(LOG_DEBUG, "ssl accept success");
                        conn.accepted = true;
                    }
                    int char_num;

                    char buffer[1000]{};
                    request.clear();
                    while ((char_num = SSL_read(conn.ssl, &buffer, 999)) > 0)
                    {
                        request.append(buffer, buffer + char_num);
                    }
                    error_code = SSL_get_error(conn.ssl, char_num);
                    if (!request.empty())
                        f(conn, request);
                }
                if (events[i].events == EPOLLRDHUP ||
                    error_code == SSL_ERROR_ZERO_RETURN || error_code == SSL_ERROR_SYSCALL || error_code == SSL_ERROR_SSL)
                {
                    char address_str[INET6_ADDRSTRLEN]{};
                    std::uint16_t port;
                    get_address_and_port(conn.addr, address_str, sizeof(address_str), port);
                    syslog(LOG_DEBUG, "remove connection from %s:%hu", address_str, port);
                    SSL_free(conn.ssl);
                    epoll_ctl(stat.epoll_fd, EPOLL_CTL_DEL, conn.fd, nullptr);
                    close(conn.fd);
                    stat.connections.return_object(id);
                }
            }
        }
    }
private:
    ssl_server_stat stat;
    std::vector<epoll_event> events;
    std::string request;
};
#endif //ENCRYPTED_COMMUNICATION_SERVER_HPP
