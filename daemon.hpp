#ifndef ENCRYPTED_COMMUNICATION_DAEMON_HPP
#define ENCRYPTED_COMMUNICATION_DAEMON_HPP
#include <sys/types.h>
#include <sys/unistd.h>
#include <cstdio>
#include <fcntl.h>
#include <cstdlib>


int daemonize()
{
    int pid;
    switch (pid = fork())
    {
        case -1:
            return -1;
        case 0:
            break;
        default:
            return pid;
    }
    if (setsid() == -1)
        return -1;
    if (chdir("/") != 0)
    {
        perror("chdir failed.");
        return -1;
    }
    int fd;
    if ((fd = open("/dev/null", O_RDWR, 0)) != -1)
    {
        if (dup2(fd, STDIN_FILENO) < 0)
        {
            perror("dup2 stdin");
            return -1;
        }
        if (dup2(fd, STDOUT_FILENO) < 0)
        {
            perror("dup2 stdout");
            return -1;
        }

        if (fd > STDERR_FILENO)
        {
            if (close(fd) < 0)
            {
                perror("close");
                return -1;
            }
        }
    }
    return 0;
}
#endif //ENCRYPTED_COMMUNICATION_DAEMON_HPP
