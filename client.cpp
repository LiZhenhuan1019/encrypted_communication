#include <iostream>
#include <string>
#include "use_ssl.hpp"


int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        std::printf("usage: client SERVER PORT\n");
        return 0;
    }
    char const *hostname = argv[1];
    char const *port = argv[2];
    use_ssl::initialize_ssl();
    SSL_CTX *ctx = use_ssl::create_client_contex();

    int fd = use_ssl::open_client(hostname, port);
    if (fd == -1)
    {
        std::fprintf(stderr, "cannot connect to server.\n");
        return 0;
    }
    SSL *ssl = SSL_new(ctx);
    if (SSL_set_fd(ssl, fd) == 0)
    {
        ERR_print_errors_fp(stderr);
        fprintf(stderr, "set fd failed.\n");
        return -1;
    }
    if (SSL_connect(ssl) <= 0)
    {
        ERR_print_errors_fp(stderr);
        fprintf(stderr, "SSL_connect failed.\n");
        return -1;
    }
    use_ssl::show_certs(ssl);
    if (!use_ssl::verify_certs_with_pubkey(ssl, "/home/lzh/.ssl/id_rsa.pub"))
    {
        fprintf(stderr, "Server identity verify failed.\n");
        SSL_shutdown(ssl);
        SSL_free(ssl);
        close(fd);
        SSL_CTX_free(ctx);
        return 0;
    }
    std::string line;
    std::cout << "To server:\n>";
    while (std::getline(std::cin, line))
    {
        SSL_write(ssl, line.c_str(), line.size());
        printf("received from server:\n<");
        char read_char;
        int char_num;
        while ((char_num = SSL_read(ssl, &read_char, 1)) == 1 && read_char != '\n')
        {
            std::putchar(read_char);
        }
        printf("\n");
        int error_code = SSL_get_error(ssl, char_num);
        if (error_code == SSL_ERROR_ZERO_RETURN || error_code == SSL_ERROR_SYSCALL || error_code == SSL_ERROR_SSL)
            break;
        std::cout << "To server:\n>";
    }

    SSL_shutdown(ssl);
    SSL_free(ssl);
    close(fd);
    SSL_CTX_free(ctx);
    return 0;
}